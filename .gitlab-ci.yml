image: docker:stable

## Helpful post regarding sequential jobs: https://about.gitlab.com/2016/07/29/the-basics-of-gitlab-ci/
stages:
  - build
  - deploy

variables:
  ##  When using dind (Docker in Docker) service we need to instruct docker, to talk with the daemon started inside of the service. The daemon is available with a network connection instead of the default /var/run/docker.sock socket.
  ##
  ## The 'docker' hostname is the alias of the service container as described at
  ## https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
  DOCKER_HOST: tcp://docker:2375/
  ## When using dind, it's wise to use the overlayfs driver for improved performance.
  DOCKER_DRIVER: overlay2
  ## Used for building, tagging, and pushing the image
  CONTAINER_IMAGE: registry.gitlab.com/$CI_PROJECT_PATH/cfml

services:
  - docker:dind

## https://stackoverflow.com/questions/49173988/how-to-get-commit-date-and-time-on-gitlab-ci
before_script:
  ## We're gonna log into the gitlab registry, as that's where these images are stored (https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor)
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  ## Needed to get the date from the commit sha, for the build tag. The stable Docker image is on Alpine Linux (https://  git-scm.com/download/linux)
  - apk add git
  ## So we can see what's going on in the logs
  - docker info
  ## Use git `show` with --format=%ci option to get ISO 8601 date stamp of commit (http://schacon.github.io/git/  git-show#_pretty_formats)
  - export COMMIT_TIME=$(git show -s --format=%ci $CI_COMMIT_SHA)
  ## Just use the first 10 characters of the timestamp (example: 2019-03-19)
  - export COMMIT_TIME_SHORT=$( echo $COMMIT_TIME | head -c10 )
  ## Combine both into a date/sha tag
  - export BUILD_TAG="${COMMIT_TIME_SHORT}_$CI_COMMIT_SHORT_SHA"

build:
  stage: build
  only:
    - deploy
  script:
    ## enable buildkit support
    - export DOCKER_BUILDKIT=1
    ## Build the image, with the build tag and the latest tag
    - docker build --tag $CONTAINER_IMAGE:$BUILD_TAG --tag $CONTAINER_IMAGE:latest -f ./build/cfml/Dockerfile .
    ## List images, so we can confirm success
    - docker image ls
    ## Push with the build tag
    - docker push $CONTAINER_IMAGE:$BUILD_TAG
    ## Push with latest
    - docker push $CONTAINER_IMAGE:latest

deploy:
  stage: deploy
  only:
    - deploy
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /Initial commit/i
      - $CI_COMMIT_MESSAGE =~ /skip deploy/i
      - $CI_COMMIT_MESSAGE =~ /don't deploy/i
  script:
    ## SSH portion is taken from https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    ## Install ssh-agent if not already installed
    - 'which ssh-agent || ( apk update && apk add openssh-client )'
    ## Run ssh-agent (inside the build environment)
    - eval $(ssh-agent -s)
    ## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store. We're using tr to fix line endings which makes ed25519 keys work without extra base64 encoding. (https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    ## Create the SSH directory and give it the right permissions
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    ## Use the contents of the SSH_KNOWN_HOSTS variable as your known hosts, so that we can SSH in
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    ## Extract the host IP from the known hosts file, so that we don't need to set it manually
    - export HOST_IP=$( grep -oE '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])' ~/.ssh/known_hosts | head -1 )
    ## Enable SSH functionality made possible in 18.0.9 to switch our context to the remote server
    - export DOCKER_HOST=ssh://root@${HOST_IP}
    ## Deploy the stack - registry auth is for gitlab
    - docker stack deploy -c docker-compose.yml -c docker-compose.prod.yml basetest --with-registry-auth