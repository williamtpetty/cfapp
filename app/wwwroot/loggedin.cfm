<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>You're Logged In</title> 
    <script src="logout.js"></script>
  </head>
  <header>
    <h1>Welcome to the home page!</h1>
    <cfscript>
      writeOutput( "<h2>Today's date is <i>#dateformat( now(), 'short' )#</i></h2>" );
    </cfscript>
  </header>
  <body>
    <h2>Your Username is: <span id="output"></span></h2>
    <script>
      if (localStorage.getItem("username") !== "username") {
        window.location.href = "index.cfm";
      } else {
        usernameOutput = document.getElementById("output").innerHTML = localStorage.getItem("username");
      }
    </script>

    <button id="logout-button" onclick="logoutFunction()">Logout</button>

  </body>
</html>