const loginForm = document.getElementById("login-form");
const submitButton = document.getElementById("submit-button");

submitButton.addEventListener("click", (event) => {
  event.preventDefault();
  const usernameInput = loginForm.username.value;
  const passwordInput = loginForm.password.value;
  const correctUsername = "username";
  const correctPassword = "password";

  if (usernameInput === correctUsername && passwordInput === correctPassword) {
    window.localStorage.setItem("username", usernameInput);
    window.location.href = "loggedin.cfm";
  } else {
    alert("Incorrect Username and/or Password");
  }
});
