<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Basic CFML Login</title>
    <script defer src="loginform.js"></script>
  </head>
  <header>
    <h1>Welcome to the login page!</h1>
    <cfscript>
      writeOutput( "<h2>Today's date is <i>#dateformat( now(), 'short' )#</i></h2>" );
    </cfscript>
    <h4>We're in a docker container</h4>
  </header>
  <body>
    <div id="error-handler">
      <p id="error-messages"></p>
    </div>

    <form id="login-form">
      <input
        style="margin-bottom: 10px"
        type="text"
        name="username"
        id="username-input"
        placeholder="Username"
      />
      <br />
      <input
        style="margin-bottom: 10px"
        type="text"
        name="password"
        placeholder="Password"
      />
      <br />
      <input
        style="margin-bottom: 10px"
        type="submit"
        name="Submit"
        id="submit-button"
        
      />
    </form>
  </body>
</html>
