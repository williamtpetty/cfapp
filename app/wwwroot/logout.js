function logoutFunction() {
  const logoutButton = document.getElementById("logout-button");

  logoutButton.addEventListener("click", (event) => {
    event.preventDefault();
    window.localStorage.removeItem("username");
    window.location.href = "index.cfm";
  });
}
